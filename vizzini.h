#ifndef VIZZINI_H
#define VIZZINI_H

#define XR_SET_REG              0

#define URM_REG_BLOCK           4
#define EPLOCALS_REG_BLOCK      0x66

#define MEM_EP_LOCALS_SIZE_S    3
#define MEM_EP_LOCALS_SIZE      (1 << MEM_EP_LOCALS_SIZE_S)

#define EP_WIDE_MODE            0x03


#define UART_GPIO_MODE                                     0x01a

#define UART_GPIO_MODE_SEL_M                               0x7
#define UART_GPIO_MODE_SEL_S                               0
#define UART_GPIO_MODE_SEL                                 0x007

#define UART_GPIO_MODE_SEL_GPIO                            (0x0 << UART_GPIO_MODE_SEL_S)
#define UART_GPIO_MODE_SEL_RTS_CTS                         (0x1 << UART_GPIO_MODE_SEL_S)
#define UART_GPIO_MODE_SEL_DTR_DSR                         (0x2 << UART_GPIO_MODE_SEL_S)

#define UART_ENABLE                                        0x003
#define UART_ENABLE_TX_M                                   0x1
#define UART_ENABLE_TX_S                                   0
#define UART_ENABLE_TX                                     0x001
#define UART_ENABLE_RX_M                                   0x1
#define UART_ENABLE_RX_S                                   1
#define UART_ENABLE_RX                                     0x002

#define UART_CLOCK_DIVISOR_0                               0x004
#define UART_CLOCK_DIVISOR_1                               0x005
#define UART_CLOCK_DIVISOR_2                               0x006

#define UART_TX_CLOCK_MASK_0                               0x007
#define UART_TX_CLOCK_MASK_1                               0x008

#define UART_RX_CLOCK_MASK_0                               0x009
#define UART_RX_CLOCK_MASK_1                               0x00a

#define UART_FORMAT                                        0x00b

#define UART_FORMAT_SIZE_M                                 0xf
#define UART_FORMAT_SIZE_S                                 0
#define UART_FORMAT_SIZE                                   0x00f

#define UART_FORMAT_SIZE_7                                 (0x7 << UART_FORMAT_SIZE_S)
#define UART_FORMAT_SIZE_8                                 (0x8 << UART_FORMAT_SIZE_S)
#define UART_FORMAT_SIZE_9                                 (0x9 << UART_FORMAT_SIZE_S)

#define UART_FORMAT_PARITY_M                               0x7
#define UART_FORMAT_PARITY_S                               4
#define UART_FORMAT_PARITY                                 0x070

#define UART_FORMAT_PARITY_NONE                            (0x0 << UART_FORMAT_PARITY_S)
#define UART_FORMAT_PARITY_ODD                             (0x1 << UART_FORMAT_PARITY_S)
#define UART_FORMAT_PARITY_EVEN                            (0x2 << UART_FORMAT_PARITY_S)
#define UART_FORMAT_PARITY_1                               (0x3 << UART_FORMAT_PARITY_S)
#define UART_FORMAT_PARITY_0                               (0x4 << UART_FORMAT_PARITY_S)

#define UART_FORMAT_STOP_M                                 0x1
#define UART_FORMAT_STOP_S                                 7
#define UART_FORMAT_STOP                                   0x080

#define UART_FORMAT_STOP_1                                 (0x0 << UART_FORMAT_STOP_S)
#define UART_FORMAT_STOP_2                                 (0x1 << UART_FORMAT_STOP_S)

#define UART_FLOW                                          0x00c

#define UART_FLOW_MODE_M                                   0x7
#define UART_FLOW_MODE_S                                   0
#define UART_FLOW_MODE                                     0x007

#define UART_FLOW_MODE_NONE                                (0x0 << UART_FLOW_MODE_S)
#define UART_FLOW_MODE_HW                                  (0x1 << UART_FLOW_MODE_S)
#define UART_FLOW_MODE_SW                                  (0x2 << UART_FLOW_MODE_S)

#define UART_XON_CHAR                                      0x010
#define UART_XOFF_CHAR                                     0x011

#define URM_ENABLE_BASE                                    0x010
#define URM_ENABLE_0                                       0x010
#define URM_ENABLE_0_TX                                    0x001
#define URM_ENABLE_0_RX                                    0x002

#ifndef CMSPAR
#define CMSPAR			0
#endif

/*
 * Major and minor numbers.
 */

#define ACM_TTY_MAJOR		166
#define ACM_TTY_MINORS		256

/*
 * Requests.
 */

#define USB_RT_ACM		(USB_TYPE_CLASS | USB_RECIP_INTERFACE)

/*
 * Output control lines.
 */

#define ACM_CTRL_DTR		0x01
#define ACM_CTRL_RTS		0x02

/*
 * Input control lines and line errors.
 */

#define ACM_CTRL_DCD		0x01
#define ACM_CTRL_DSR		0x02
#define ACM_CTRL_BRK		0x04
#define ACM_CTRL_RI		0x08

#define ACM_CTRL_FRAMING	0x10
#define ACM_CTRL_PARITY		0x20
#define ACM_CTRL_OVERRUN	0x40

/*
 * Internal driver structures.
 */

/*
 * The only reason to have several buffers is to accommodate assumptions
 * in line disciplines. They ask for empty space amount, receive our URB size,
 * and proceed to issue several 1-character writes, assuming they will fit.
 * The very first write takes a complete URB. Fortunately, this only happens
 * when processing onlcr, so we only need 2 buffers. These values must be
 * powers of 2.
 */
#define ACM_NW  16
#define ACM_NR  16

struct acm_wb {
	unsigned char *buf;
	dma_addr_t dmah;
	int len;
	int use;
	struct urb		*urb;
	struct acm		*instance;
};

struct acm_rb {
	int			size;
	unsigned char		*base;
	dma_addr_t		dma;
	int			index;
	struct acm		*instance;
};

struct acm {
	struct usb_device *dev;				/* the corresponding usb device */
	struct usb_interface *control;			/* control interface */
	struct usb_interface *data;			/* data interface */
	struct tty_port port;			 	/* our tty port data */
	struct urb *ctrlurb;				/* urbs */
	u8 *ctrl_buffer;				/* buffers of urbs */
	dma_addr_t ctrl_dma;				/* dma handles of buffers */
	u8 *country_codes;				/* country codes from device */
	unsigned int country_code_size;			/* size of this buffer */
	unsigned int country_rel_date;			/* release date of version */
	struct acm_wb wb[ACM_NW];
	unsigned long read_urbs_free;
	struct urb *read_urbs[ACM_NR];
	struct acm_rb read_buffers[ACM_NR];
	int rx_buflimit;
	int rx_endpoint;
	spinlock_t read_lock;
	int write_used;					/* number of non-empty write buffers */
	int transmitting;
	spinlock_t write_lock;
	struct mutex mutex;
	bool disconnected;
	struct usb_cdc_line_coding line;		/* bits, stop, parity */
	struct work_struct work;			/* work queue entry for line discipline waking up */
	unsigned int ctrlin;				/* input control lines (DCD, DSR, RI, break, overruns) */
	unsigned int ctrlout;				/* output control lines (DTR, RTS) */
	struct async_icount iocount;			/* counters for control line changes */
	struct async_icount oldcount;			/* for comparison of counter */
	wait_queue_head_t wioctl;			/* for ioctl */
	unsigned int writesize;				/* max packet size for the output bulk endpoint */
	unsigned int readsize,ctrlsize;			/* buffer sizes for freeing */
	unsigned int minor;				/* acm minor number */
	unsigned char clocal;				/* termios CLOCAL */
	unsigned int ctrl_caps;				/* control capabilities from the class specific header */
	unsigned int susp_count;			/* number of suspended interfaces */
	unsigned int combined_interfaces:1;		/* control and data collapsed */
	unsigned int is_int_ep:1;			/* interrupt endpoints contrary to spec used */
	unsigned int throttled:1;			/* actually throttled */
	unsigned int throttle_req:1;			/* throttle requested */
	u8 bInterval;
	struct usb_anchor delayed;			/* writes queued for a device about to be woken */
	unsigned long quirks;

        int           block;

};

#define CDC_DATA_INTERFACE_TYPE	0x0a

/* constants describing various quirks and errors */
#define NO_UNION_NORMAL			BIT(0)
#define SINGLE_RX_URB			BIT(1)
#define NO_CAP_LINE			BIT(2)
#define NO_DATA_INTERFACE		BIT(4)
#define IGNORE_DEVICE			BIT(5)
#define QUIRK_CONTROL_LINE_STATE	BIT(6)
#define CLEAR_HALT_CONDITIONS		BIT(7)

#endif /*VIZZINI_H*/
